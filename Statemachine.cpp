#include "Statemachine.h"

#include "Input.h"
#include "Output.h"
#include <stdbool.h>
#include <stdio.h>
#include <time.h>

#include "measurements.h"

#include <cstdlib>
#include <vector>
#include <list>

using namespace std;

enum states {
    initState, initStateStartPressed, state2, state2StartPressed, transStart, messung, messungOk, messungFehler,
    weiche, weicheKeinMetall, weicheMetall, rutsche, auslauf, nullState
};

static bool merker = 0;
static enum states aktuellerZustand = initState;
static bool Q1 = false;

// Stativ variablen für stateTime
static enum states timeState = nullState;
static int timeOfTheState = 0;

void evalStatemachine(vector <measurements>& listOfMeasurements, vector <measurements>::iterator& currentMeasurement) {

    unsigned int bitOutputMask = 0;
    unsigned int bitOutputClearMask = 0;

    unsigned int bitInputMask = 0;

    switch (aktuellerZustand) {
        case initState:
            bitOutputClearMask = WEICHEAUF | AMPELGRUEN | AMPELGELB | AMPELROT | LEDQ1 | LEDQ2;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | LEDSTARTTASTE;
            setBitInOutput(bitOutputMask);
            break;
        case initStateStartPressed:

            bitOutputClearMask = WEICHEAUF | AMPELGRUEN | AMPELGELB | AMPELROT | LEDQ1 | LEDQ2;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | LEDSTARTTASTE;
            setBitInOutput(bitOutputMask);
            break;
        case state2:

            bitOutputClearMask = LEDSTARTTASTE | AMPELROT;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELGRUEN;
            setBitInOutput(bitOutputMask);
            break;
        case state2StartPressed:

            bitOutputClearMask = LEDSTARTTASTE;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELGRUEN;
            setBitInOutput(bitOutputMask);
            break;
        case transStart:

            bitOutputClearMask = AMPELGRUEN | MOTORSTOP | LEDQ1;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORRECHTSLAUF | AMPELGELB;
            setBitInOutput(bitOutputMask);
            break;
        case messung:

            bitOutputClearMask = MOTORRECHTSLAUF | AMPELGELB;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
        case messungOk:

            bitOutputClearMask = MOTORSTOP | AMPELROT;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORRECHTSLAUF | MOTORLANGSAM | AMPELGRUEN | LEDQ1;
            setBitInOutput(bitOutputMask);
            break;
        case messungFehler:

            bitOutputClearMask = MOTORSTOP;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORRECHTSLAUF | MOTORLANGSAM | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;

        case weiche:

            bitOutputClearMask = MOTORLANGSAM | MOTORRECHTSLAUF | AMPELGRUEN | AMPELROT;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
        case weicheKeinMetall:

            bitOutputClearMask = MOTORSTOP | AMPELROT;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORRECHTSLAUF | AMPELGELB | WEICHEAUF | LEDQ2;
            setBitInOutput(bitOutputMask);
            break;
        case weicheMetall:

            bitOutputClearMask = MOTORSTOP;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORRECHTSLAUF | AMPELGELB | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
        case auslauf:

            bitOutputClearMask = LEDQ1 | MOTORRECHTSLAUF | AMPELGELB | WEICHEAUF | LEDQ2;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELGRUEN;
            setBitInOutput(bitOutputMask);
            break;
        case rutsche:

            bitOutputClearMask = MOTORRECHTSLAUF | AMPELGELB | AMPELROT | LEDQ1 | LEDQ2;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
    }
    switch (aktuellerZustand) {

        case initState:
            bitInputMask = TASTESTART | KEINEINLAUFWERKSTUECK | KEINWERKSTUECKINHOEHENMESSUNG | KEINWERKSTUECKINWEICHE | RUTSCHELEER | KEINWERKSTUECKIMAUSLAUF | TASTESTOP | ESTOP;
            merker = 0;
            if (isBitMaskSet(bitInputMask)) {
                aktuellerZustand = state2StartPressed;
                // Liste leeren
                listOfMeasurements.clear();
            }
            break;
        case initStateStartPressed:
            if (stateTime() > 1 && !isBitSet(TASTESTART)) {
                aktuellerZustand = initState;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
        case state2:
            if (!isBitSet(KEINEINLAUFWERKSTUECK) && isBitSet(RUTSCHELEER)) {
                aktuellerZustand = transStart;
                // Ein neues Objekt anlegen
                measurements actualMeasurement;
                // Das Objekt in die Liste packen
                listOfMeasurements.push_back(actualMeasurement);
                // Wenn nur ein Objekt in der Liste ist, wird der Iterator auf den beginn gesetzt,
                // ansonten wird der iterator um 1 weiter gezählt
                currentMeasurement = listOfMeasurements.begin() + listOfMeasurements.size() - 1;
                // Anfangszeit in Objekt speichern
                (*currentMeasurement).setStartzeit();
            } else if (isBitSet(TASTESTART)) {
                aktuellerZustand = initStateStartPressed;
                // Gesamtanzahl auf dem Bildschirm ausgeben
                printf("Gesamtanzahl: %d\n", listOfMeasurements.size());
                // Werte auf dem Bildschirm ausgeben
                for (std::vector<measurements>::iterator current = listOfMeasurements.begin(); current != listOfMeasurements.end(); current++) {
                    (*current).werteAufDemBildschirmAusgeben();
                }

                // Liste in die Datei speichern
                // Neuen iterator anlegen und mit diesem in einer Schleife die Werte durchlaufen und Objekte abspeichern
                // datafile muss ein pointer auf ein File sein, der string in ofopen zum schreiben muss mit einer variable gestaltet werden
                if (listOfMeasurements.size() != 0) {

                    FILE* datafile;
                    char dateiName[100];

                    void DateinameGenerieren(dateiName);

                    datafile = fopen(dateiName, "w");

                    for (std::vector<measurements>::iterator current = listOfMeasurements.begin(); current != listOfMeasurements.end(); current++) {
                        current->writeToFile(datafile);
                    }
                    fclose(datafile);
                }
                // Nächsten Dateinamen angeben, damit keine Datei überschrieben wird

            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case state2StartPressed:
            if (!isBitSet(KEINEINLAUFWERKSTUECK) && isBitSet(RUTSCHELEER)) {
                aktuellerZustand = transStart;
                // Ein neues Objekt anlegen
                measurements actualMeasurement;
                // Das Objekt in die Liste packen
                listOfMeasurements.push_back(actualMeasurement);
                // Wenn nur ein Objekt in der Liste ist, wird der Iterator auf den beginn gesetzt,
                // ansonten wird der iterator um 1 weiter gezählt
                currentMeasurement = listOfMeasurements.begin() + listOfMeasurements.size() - 1;
                // Anfangszeit in Objekt speichern
                (*currentMeasurement).setStartzeit();
            } else if (stateTime() > 1 && !isBitSet(TASTESTART)) {
                aktuellerZustand = state2;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case transStart:
            if (!isBitSet(KEINWERKSTUECKINHOEHENMESSUNG)) {
                aktuellerZustand = messung;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case messung:
            if (isBitSet(WERKSTUECKHOEHEOK)) {
                aktuellerZustand = messungOk;
                // Messung ist ok in das Objekt schreiben
                currentMeasurement->setHoehe(true);
            } else if (!isBitSet(WERKSTUECKHOEHEOK)) {
                aktuellerZustand = messungFehler;
                // Messung ist nicht ok in das Objekt schreiben
                currentMeasurement->setHoehe(false);
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case messungOk:

            if (!isBitSet(KEINWERKSTUECKINWEICHE)) {
                aktuellerZustand = weiche;
                Q1 = true;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
        case messungFehler:

            if (!isBitSet(KEINWERKSTUECKINWEICHE)) {
                aktuellerZustand = weiche;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case weiche:

            if (isBitSet(WERKSTUECKMETALL)) {
                aktuellerZustand = weicheMetall;
                // Werkstück ist aus Metall in Objekt speichern
                currentMeasurement->setMetall(true);
            } else if (!isBitSet(WERKSTUECKMETALL)) {
                aktuellerZustand = weicheKeinMetall;
                // Werkstück ist nicht aus Metall in Objekt speichern currentMeasurement->setHoehe(false)
                currentMeasurement->setMetall(false);
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case weicheMetall:
            if (!isBitSet(RUTSCHELEER)) {
                aktuellerZustand = rutsche;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
        case weicheKeinMetall:

            if (!isBitSet(KEINWERKSTUECKIMAUSLAUF)) {
                aktuellerZustand = auslauf;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
        case rutsche:

            if (!isBitSet(RUTSCHELEER) && isBitSet(KEINWERKSTUECKINWEICHE)) {
                aktuellerZustand = state2;
                // Endzeit in Objekt speichern
                (*currentMeasurement).setZeitEnde();
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
        case auslauf:

            if (isBitSet(KEINWERKSTUECKIMAUSLAUF)) {
                aktuellerZustand = state2;
                // Endzeit in Objekt speichern
                (*currentMeasurement).setZeitEnde();
            } else if (STOP) {
                aktuellerZustand = initState;
            }
    }
}

int stateTime() {

    int var;

    // Wird der Zustand gewechselt, wird die Zeit auf Null gesetzt
    if (aktuellerZustand != timeState) {
        timeOfTheState = time(NULL);
        timeState = aktuellerZustand;
    }

    var = (time(NULL) - timeOfTheState);
    return var;

}

void DateinameGenerieren(char &dateiName[100]) {

    strcpy(dateiName, "Messdaten Der Werkstuecke ");

    time_t dataTime;
    time(&dataTime);
    string zugeschnitteneUhrzeit = ctime(&dataTime);
    zugeschnitteneUhrzeit.erase(0, 11);
    zugeschnitteneUhrzeit.erase(8, 12);
    zugeschnitteneUhrzeit.replace(2, 1, "-", 0, 1);
    zugeschnitteneUhrzeit.replace(5, 1, "-", 0, 1);

    strncat(dateiName, zugeschnitteneUhrzeit.c_str(), 27);

    strncat(dateiName, ".csv", 34);
}