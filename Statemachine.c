#include "Statemachine.h"

#include "Input.h"
#include "Output.h"
#include <stdbool.h>
#include <stdio.h>
#include <time.h>

#include <cstdlib>
#include <vector>

using namespace std;

enum states {
    initState, initStateStartPressed, state2,state2StartPressed, transStart, messung, messungOk, messungFehler,
    weiche, weicheKeinMetall, weicheMetall, rutsche, auslauf
};
static bool merker = 0;
static enum states aktuellerZustand = initState;
static bool Q1 = false;

void evalStatemachine(vector <measuremets>& listOfMeasuremets, vector <measurements>::iterator& currentMeasurement) {
    
    unsigned int bitOutputMask = 0;
    unsigned int bitOutputClearMask = 0;
    
    switch (aktuellerZustand) {
        case initState:
            bitOutputClearMask = WEICHEAUF | AMPELGRUEN | AMPELGELB | AMPELROT | LEDQ1 | LEDQ2;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORSTOP | LEDSTARTTASTE;
            setBitInOutput(bitOutputMask);
            break;
        case initStateStartPressed:
            
            bitOutputClearMask = WEICHEAUF | AMPELGRUEN | AMPELGELB | AMPELROT | LEDQ1 | LEDQ2;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORSTOP | LEDSTARTTASTE;
            setBitInOutput(bitOutputMask);
            break;
        case state2:
            
            bitOutputClearMask = LEDSTARTTASTE;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORSTOP | AMPELGRUEN;
            setBitInOutput(bitOutputMask);
            break;
        case state2StartPressed:
            
            bitOutputClearMask = LEDSTARTTASTE;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORSTOP | AMPELGRUEN;
            setBitInOutput(bitOutputMask);
            break;
        case transStart:
            
            bitOutputClearMask = AMPELGRUEN | MOTORSTOP | LEDQ1;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORRECHTSLAUF | AMPELGELB;
            setBitInOutput(bitOutputMask);
            break;
        case messung:
            
            bitOutputClearMask = MOTORRECHTSLAUF | AMPELGELB;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORSTOP | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
        case messungOk:
            
            bitOutputClearMask = MOTORSTOP | AMPELROT;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORRECHTSLAUF | MOTORLANGSAM | AMPELGRUEN | LEDQ1;
            setBitInOutput(bitOutputMask);
            break;
        case messungFehler:
            
            bitOutputClearMask = MOTORSTOP;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORRECHTSLAUF | MOTORLANGSAM | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;

        case weiche:
            
            bitOutputClearMask = MOTORLANGSAM | MOTORRECHTSLAUF | AMPELGRUEN;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORSTOP | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
        case weicheKeinMetall:
            
            bitOutputClearMask = MOTORSTOP | AMPELROT;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORRECHTSLAUF | AMPELGELB | WEICHEAUF | LEDQ2;
            setBitInOutput(bitOutputMask);
            break;
        case weicheMetall:
            
            bitOutputClearMask = MOTORSTOP;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORRECHTSLAUF | AMPELGELB | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
        case auslauf:
            
            bitOutputClearMask = LEDQ2 | MOTORRECHTSLAUF | AMPELGELB | WEICHEAUF;
            clearBitInOutput(bitOutputClearMask);
 
            bitOutputMask = MOTORSTOP | AMPELGRUEN;
            setBitInOutput(bitOutputMask);
            break;
        case rutsche:
            
            bitOutputClearMask = MOTORRECHTSLAUF | AMPELGELB;
            clearBitInOutput(bitOutputClearMask);
            
            bitOutputMask = MOTORSTOP | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
    }
    switch (aktuellerZustand) {
            unsigned int bitInputMask = 0;
        case initState:
            bitInputMask = TASTESTART | KEINEINLAUFWERKSTUECK | KEINWERKSTUECKINHOEHENMESSUNG | KEINWERKSTUECKINWEICHE | RUTSCHELEER | KEINWERKSTUECKIMAUSLAUF | TASTESTOP | ESTOP;
            merker = 0;
            if (isBitMaskSet(bitInputMask)) {
                aktuellerZustand = state2StartPressed;
            }
            break;
        case initStateStartPressed:
            if (stateTime() > 1 && !isBitSet(TASTESTART)){
                aktuellerZustand = initState;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
        case state2:
            if (!isBitSet(KEINEINLAUFWERKSTUECK) && isBitSet(RUTSCHELEER)) {
                aktuellerZustand = transStart;
            } else if (isBitSet(TASTESTART)) {
                aktuellerZustand = initStateStartPressed;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
            
        case state2StartPressed:
            if (!isBitSet(KEINEINLAUFWERKSTUECK) && isBitSet(RUTSCHELEER)) {
                aktuellerZustand = transStart;
                // Ein neues Objekt anlegen
                measurements actualMeasurement;
                // Das Objekt in die Liste packen
                listOfMeasurements.push_back(actualMeasurement);               
                // Wenn nur ein Objekt in der Liste ist, wird der Iterator auf den beginn gesetzt,
                // ansonten wird der iterator um 1 weiter gezählt
                if(listOfMeasurements.size() == 1){
                    currentMeasurement = listOfMeasurements.begin();
                }   else {
                    currentMeasurement++;
                }                 
            } else if (stateTime() > 1 && !isBitSet(TASTESTART)) {
                aktuellerZustand = state2;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case transStart:
            if (!isBitSet(KEINWERKSTUECKINHOEHENMESSUNG)) {
                aktuellerZustand = messung;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case messung:
            if (isBitSet(WERKSTUECKHOEHEOK)) {
                aktuellerZustand = messungOk;
                printf("Messung ist ok\n");

            } else if (!isBitSet(WERKSTUECKHOEHEOK)) {
                aktuellerZustand = messungFehler;
                printf("Messung ist nicht ok\n");
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case messungOk:

            if (!isBitSet(KEINWERKSTUECKINWEICHE)) {
                aktuellerZustand = weiche;
                Q1 = true;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
        case messungFehler:

            if (!isBitSet(KEINWERKSTUECKINWEICHE)) {
                aktuellerZustand = weiche;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case weiche:

            if (isBitSet(WERKSTUECKMETALL)) {
                aktuellerZustand = weicheMetall;
            } else if (!isBitSet(WERKSTUECKMETALL)) {
                aktuellerZustand = weicheKeinMetall;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;

        case weicheMetall:
            if (!isBitSet(RUTSCHELEER)) {
                aktuellerZustand = rutsche;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
        case weicheKeinMetall:

            if (!isBitSet(KEINWERKSTUECKIMAUSLAUF)) {
                aktuellerZustand = auslauf;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
        case rutsche:

            if (!isBitSet(RUTSCHELEER) && isBitSet(KEINWERKSTUECKINWEICHE)) {
                aktuellerZustand = state2;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
            break;
        case auslauf:

            if (isBitSet(KEINWERKSTUECKIMAUSLAUF)) {
                aktuellerZustand = state2;
            } else if (STOP) {
                aktuellerZustand = initState;
            }
    }
}


int stateTime() { 

   // static enum states timeState = state;
   static enum states timeState = state2;
   static int timeOfTheState = 0;
   int var;
    
    // Wird der Zustand gewechselt, wird die Zeit auf Null gesetzt
    if (aktuellerZustand != timeState){
        timeOfTheState = time(NULL);
        timeState = aktuellerZustand;
    }
   
   var = (time(NULL) - timeOfTheState);
   return var;
     
}