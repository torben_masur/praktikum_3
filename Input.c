#include "Input.h"
#include "cbw.h"

static unsigned int ProcessImage = 0;

/*Eine Prozedur updateProcessImage, die die aktuellen Sensorsignale aus den 
 Eingabe-Ports einliest und im internen Prozessabbild ablegt*/
void updateProcessImage() {
    short unsigned int PortB = 0;
    short unsigned int PortCH = 0;
    cbDIn(0, FIRSTPORTCH, &PortCH);
    cbDIn(0, FIRSTPORTB, &PortB);
    ProcessImage = (PortCH << 8) | PortB;
    return;
}

/*Die Funktion istBitSet bekommt eine Bit-Maske übergeben. Sie prüft, ob die
 durch die Bit-Maske angegebenen Bit im Prozessabbild gesetzt sind. Rückgabewert
 ist true (alle angegebenen Bit gesetzt) oder false*/
bool isBitMaskSet(int bitInputMask) {
    bool isBitMaskSet = false;
    if (ProcessImage == bitInputMask) {
        isBitMaskSet = true;
    }
    return (isBitMaskSet);

}

bool isBitSet(int checkBit) {
    bool isBitSet = false;
    if ((checkBit & ProcessImage) == checkBit) {
        isBitSet = true;
    }
    return (isBitSet);
}

void intialisierung() {
    cbDConfigPort(0, FIRSTPORTB, DIGITALIN);
    cbDConfigPort(0, FIRSTPORTCH, DIGITALIN);
    cbDConfigPort(0, FIRSTPORTA, DIGITALOUT);
    cbDConfigPort(0, FIRSTPORTCL, DIGITALOUT);
}


