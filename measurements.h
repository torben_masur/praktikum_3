/* 
 * File:   measurements.h
 * Author: abz259
 *
 * Created on 13. Mai 2016, 14:41
 */

#define MAXSTRINGLENGTH 30

#include <string>


#ifndef MEASUREMENTS_H
#define	MEASUREMENTS_H

using namespace std;

class measurements {

public:
    string startzeit;
    bool hoehe;
    bool metall;
    string endzeit;
    
public:
    measurements();
    measurements(string startzeit, bool hoehe, bool metall, string endzeit);
    // Copy Konstruktor
    // Destruktor
    
    bool readFromFile(FILE* Eingabe);
    bool writeToFile(FILE* Ausgabe, char trennung = ';');
    
    void werteAufDemBildschirmAusgeben(string trennung = " ; ");
    
    void setStartzeit();
    void setStartzeit(string startzeit);
    void setHoehe(bool hoehe);
    void setMetall(bool metall);
    void setZeitEnde();
    void setZeitEnde(string endzeit);
    
    string getStartzeit();
    bool getHoehe();
    bool getMetall(); 
    string getZeitEnde();
    
};



#endif	/* MEASUREMENTS_H */