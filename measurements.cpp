#include "measurements.h"
#include <time.h>
#include <string>
#include <cstring>
#include <stdio.h>
#include <iostream>

using namespace std;

measurements::measurements() {
    this->startzeit = "0";
    this->hoehe = 0;
    this->metall = 0;
    this->endzeit = "0";
}

measurements::measurements(string startzeit, bool hoehe, bool metall, string endzeit) {
    this->startzeit = startzeit;
    this->hoehe = hoehe;
    this->metall = metall;
    this->endzeit = endzeit;
}

void measurements::setStartzeit() {
    time_t currentTime;
    time(&currentTime);
    startzeit = ctime(&currentTime);
    startzeit.erase(startzeit.end()-1);
}

void measurements::setStartzeit(string startzeit){
    this->startzeit = startzeit;
}

void measurements::setHoehe(bool hoehe) {
    this->hoehe = hoehe;
}

void measurements::setMetall(bool metall) {
    this->metall = metall;
}

void measurements::setZeitEnde() {
    time_t currentTime;
    time(&currentTime);
    endzeit = ctime(&currentTime);
    endzeit.erase(endzeit.end()-1);
}

void measurements::setStartzeit(string startzeit){
    this->endzeit = endzeit;
}

string measurements::getStartzeit() {
    return startzeit;
}

bool measurements::getHoehe() {
    return hoehe;
}

bool measurements::getMetall() {
    return metall;
}

string measurements::getZeitEnde() {
    return endzeit;
}

bool measurements::readFromFile(FILE* Eingabe) {
    /*
    int number = 0;
    if (NULL != Eingabe) {
        number = fscanf(Eingabe, "%s ; %s ; %s ; %s \n", &startzeit, &hoehe, &metall, &endzeit);
        if (number != 4) {
            startzeit = 0.0;
            hoehe = false;
            metall = false;
            endzeit = 0.0;
        }
    }
     * */
}

bool measurements::writeToFile(FILE* Ausgabe, char trennung) {
    
    char  startzeitCharArray[MAXSTRINGLENGTH];
    strcpy(startzeitCharArray, startzeit.c_str());
    char  endzeitCharArray[MAXSTRINGLENGTH];
    strcpy(endzeitCharArray, endzeit.c_str());
    char hoeheCharArray[13];
    char metallCharArray[11];
            
    if(hoehe == 0) {
       strcpy(hoeheCharArray, "Hoehe NOT OK") ;
    } else if (hoehe == 1){
       strcpy(hoeheCharArray, "Hoehe OK    ") ;
    }
    if(metall == 1) {
        strcpy(metallCharArray, "Metall    ");
    }else if (metall == 0) {
        strcpy(metallCharArray, "NOT Metall");
    }
    
    
    int number = 0;
    if (NULL != Ausgabe) {
        number = fprintf(Ausgabe,"%s %c %s %c %s %c %s \n", startzeitCharArray, trennung, hoeheCharArray, trennung, metallCharArray, trennung, endzeitCharArray);
    }

}

void measurements::werteAufDemBildschirmAusgeben(string trennung ) {
    string shoehe;
    string smetall;
            
    if(hoehe == 0) {
       shoehe = "Hoehe NOT OK" ;
    } else if(hoehe == 1){
       shoehe = "Hoehe OK    " ;
    }
    if(metall == 1) {
        smetall = "Metall    ";
    }else if(metall == 0){
        smetall = "NOT Metall";
    }
    
    cout << startzeit << trennung << shoehe << trennung << smetall << trennung << endzeit << endl;
    //printf("%s %c %d %c %d %c %d\n", startzeitCharArray, trennung, hoeheInt, trennung, metallInt, trennung, endzeitCharArray);
    
}