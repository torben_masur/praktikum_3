/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Test.h
 * Author: torbe
 *
 * Created on 24. Mai 2016, 10:23
 */

#ifndef TEST_H
#define TEST_H

class Test {
public:
    
    Test();
    Test(const Test& orig);
    virtual ~Test();
    
    testStart();
    testMeasurements();
    testList();
    testBildschirmausgabe();
    testDateispeicherung();
    
private:

};

#endif /* TEST_H */

