#ifndef STATEMACHINE_H
#define	STATEMACHINE_H
#define STOP 0

#include <vector>
#include "measurements.h"

void evalStatemachine(vector <measurements>& listOfMeasurements, vector <measurements>::iterator& currentMeasurement);
int stateTime();
void DateinameGenerieren(char &dateiName[100]);

#endif	/* STATEMACHINE_H */

