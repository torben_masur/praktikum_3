#include<stdbool.h>


#define KEINEINLAUFWERKSTUECK           ( 1 << 0 )
#define KEINWERKSTUECKINHOEHENMESSUNG   ( 1 << 1 )
#define WERKSTUECKHOEHEOK               ( 1 << 2 )
#define KEINWERKSTUECKINWEICHE          ( 1 << 3 )
#define WERKSTUECKMETALL                ( 1 << 4 )
#define WEICHEOFFEN                     ( 1 << 5 )
#define RUTSCHELEER                     ( 1 << 6 )
#define KEINWERKSTUECKIMAUSLAUF         ( 1 << 7 )
#define TASTESTART                      ( 1 << 8 )
#define TASTESTOP                       ( 1 << 9 )
#define TASTERESET                      ( 1 << 10 )
#define ESTOP                           ( 1 << 11 )

#ifndef INPUT_H
#define	INPUT_H

#ifdef	__cplusplus
extern "C" {
#endif
    void updateProcessImage();
    bool isBitMaskSet(int bitInputMask);
    bool isBitSet(int checkBit);
    void intialisierung();
#ifdef	__cplusplus
}
#endif

#endif	/* INPUT_H */

