/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ti_asssertion.h
 * Author: torbe
 *
 * Created on 24. Mai 2016, 10:46
 */

#ifndef TI_ASSSERTION_H
#define TI_ASSSERTION_H

#include <stdio.h>

#define ASSERT(e) do{ \
(e) ? 0 : printf("assertion fail %s, in %s, line %i\n", #e, __FILE__, __LINE__); \
} while(0)

#define ASSERT_M(m,e) do{ \
(e) ? 0 : printf(m ", assertion fail %s, in %s, line %i\n", #e, __FILE__, __LINE__); \
} while(0)

#endif /* TI_ASSSERTION_H */

