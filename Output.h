#define MOTORRECHTSLAUF         ( 1 << 0 )
#define MOTORLINKSLAUF          ( 1 << 1 )
#define MOTORLANGSAM            ( 1 << 2 )
#define MOTORSTOP               ( 1 << 3 )
#define WEICHEAUF               ( 1 << 4 )
#define AMPELGRUEN              ( 1 << 5 )
#define AMPELGELB               ( 1 << 6 )
#define AMPELROT                ( 1 << 7 )
#define LEDSTARTTASTE           ( 1 << 8 )
#define LEDRESETTASTE           ( 1 << 9 )
#define LEDQ1                   ( 1 << 10 )
#define LEDQ2                   ( 1 << 11 )
#define restore 0

#ifndef OUTPUT_H
#define	OUTPUT_H

#ifdef	__cplusplus
extern "C" {
#endif

    void applyOutputToProcess();
    void setBitInOutput(int bitOutputMask);
    void clearBitInOutput(int bitOutputMask);
    void resetOutputs();

#ifdef	__cplusplus
}
#endif

#endif	/* OUTPUT_H */

