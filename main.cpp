/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: torbe
 *
 * Created on 14. Mai 2016, 14:18
 */

#include <cstdlib>
#include "Input.h"
#include "Output.h"
#include "Statemachine.h"
#include "measurements.h"
#include <vector>
#include <list>

using namespace std;

int main(int argc, char** argv) {

    intialisierung();
    
    vector <measurements> listOfMeasurements;    
    vector <measurements>::iterator currentMeasurement;  
    
    while(true){
    
    updateProcessImage();
   
     // Mit pointern die Liste und den iterator übergeben
    // datafile vllt noch mit übergeben?
    evalStatemachine(listOfMeasurements, currentMeasurement);
    applyOutputToProcess(); 
    }
    return (0);

}

